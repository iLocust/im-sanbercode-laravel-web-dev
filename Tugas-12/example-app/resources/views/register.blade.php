
<!DOCTYPE html>
<html>
  <head>
    <title>Form</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>

    <form>
      <label for="fname">First name:</label><br />
      <input type="text" id="fname" name="fname" /><br />
      <label for="lname">Last name:</label><br />
      <input type="text" id="lname" name="lname" />
    </form>

    <br />
    <form action="">
      <label for="gender">Gender:</label>
      <br />
      <input type="radio" id="male" />
      <label for="html">Male</label><br />
      <input type="radio" id="female" />
      <label for="css">Female</label><br />
      <input type="radio" id="other" />
      <label for="javascript">Other</label>
    </form>
    <br />

    <form action="">
      <label for="nationality">Nationality</label>
      <br />
      <select name="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="singapora">Singapora</option>
        <option value="malaysia">Malaysia</option>
        <option value="australia">Australia</option>
      </select>
      <br /><br />
    </form>

    <form action="">
        <label for="">Bio</label>
        <br>
        <textarea name="Bio" id="bio" cols="30" rows="10"></textarea>
    </form>

    <form method=" " action="{{ url('/welcome') }}">
        <input type="submit" value="Sign Up">
      </form>
      
  </body>
</html>