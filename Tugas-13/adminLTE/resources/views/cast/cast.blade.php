@extends('layout.master')

@section('content')
    <div class="container my-5">
        <h1 class="text-center mb-5">Daftar Pemain</h1>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($cast as $key => $value)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $value->nama }}</td>
                                <td>

                                    <form action="/cast/{{ $value->id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <a href="/cast/{{ $value->id }}" class="btn btn-info btn-sm">Detail</a>
                                        <a href="/cast/{{ $value->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3">Tidak ada pemain</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                <div class="col-12 text-center">
                    <a href="/cast/create" class="btn btn-primary btn-lg" role="button">Tambah Pemain Baru</a>
                  </div>
            </div>
        </div>
    </div>
@endsection
