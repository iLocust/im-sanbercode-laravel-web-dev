@extends('layout.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <h5 class="card-title">Tambah Pemain Baru</h5>
        </div>
        <div class="card-body">
            <form action="/cast/{{$cast->id}}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="namaInput">Nama Pemain</label>
                    <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" placeholder="Masukkan Nama Pemain">
                </div>
                @error('nama')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="umurInput">Umur</label>
                    <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" placeholder="Masukkan Umur">
                </div>
                @error('umur')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="bioInput">Bio</label>
                    <textarea class="form-control" name="bio" rows="3" placeholder="Masukkan Bio">{{$cast->bio}}</textarea>
                </div>
                @error('bio')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
