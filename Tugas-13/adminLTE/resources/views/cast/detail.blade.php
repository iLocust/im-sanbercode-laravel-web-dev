@extends('layout.master')

@section('content')
<div class="container my-5">
    <div class="row">
      <div class="col-12">
        <h1 class="text-center mb-5">Profil {{$cast->nama}}</h1>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-6 mb-4">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Nama dan Umur</h5>
            <p class="card-text">{{$cast->nama}} , {{$cast->umur}} Tahun</p>
          </div>
        </div>
      </div>

      <div class="col-lg-6 mb-4">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Bio</h5>
            <p class="card-text">{{$cast->bio}}</p>
          </div>
        </div>
      </div>
    </div>
  </div>

    <a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>
@endsection
